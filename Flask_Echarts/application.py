# coding:utf-8
import MySQLdb
from flask import Flask  # 默认
from werkzeug.contrib.cache import MemcachedCache  # 引入缓存

# app入口定义
app = Flask(__name__)
# 链接数据库
conn = MySQLdb.connect(host='localhost', user='root', passwd='root', db='zp')
cache = MemcachedCache(['127.0.0.1:11211'])
